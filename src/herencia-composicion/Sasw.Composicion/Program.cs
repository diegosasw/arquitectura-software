﻿using System;

namespace Sasw.Composicion
{
    class Program
    {
        static void Main(string[] args)
        {
            var kickFight = new KickFight();
            var warrior = new Warrior(kickFight);
            warrior.ShowSkills();
        }
    }

    class KickFight
    {
        public void Fight()
        {
            Console.WriteLine("Toma patada!");
        }
    }

    class Warrior
    {
        private readonly KickFight _kickFight;

        public Warrior(KickFight kickFight)
        {
            _kickFight = kickFight;
        }

        public void ShowSkills()
        {
            _kickFight.Fight();
        }
    }
}