using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using WebApi.Contracts;
using WebApi.FunctionalTests.TestSupport;
using WebApi.FunctionalTests.TestSupport.Extensions;
using WebApi.Models;
using Xunit;

namespace WebApi.FunctionalTests.UseCases.SoccerTeamsTests
{
    public static class GetTeamsTests
    {
        public class Given_A_Url_When_Getting_Teams
            : FunctionalTest
        {
            private string _url;
            private HttpResponseMessage _result;

            protected override Task Given()
            {
                _url = "api/soccerTeams";
                return Task.CompletedTask;
            }

            protected override void ConfigureTestServices(IServiceCollection services)
            {
                base.ConfigureTestServices(services);
                services.Replace<ITeamService>(sp =>
                {
                    var miMock = new Mock<ITeamService>();
                    miMock
                        .Setup(x => x.GetTeams())
                        .ReturnsAsync(new List<Team>
                        {
                            new Team("Deportivo Alaves", 1920)
                        });

                    return miMock.Object;
                }, ServiceLifetime.Singleton);
            }

            protected override async Task When()
            {
                _result = await HttpClient.GetAsync(_url);
            }

            [Fact]
            public void Then_It_Should_Return_200_Ok()
            {
                _result.StatusCode.Should().Be(HttpStatusCode.OK);
            }
            
            [Fact]
            public async Task Then_It_Should_Only_One_Team()
            {
                var json = await _result.Content.ReadAsStringAsync();
                var teams = JsonSerializer.Deserialize<List<Team>>(json);
                teams?.Count.Should().Be(1);
            }
        }
    }
}