namespace WebApi.Models
{
    public class Team
    {
        public string Name { get; }
        public int FoundationYear { get; }

        public Team(string name, int foundationYear)
        {
            Name = name;
            FoundationYear = foundationYear;
        }
    }
}