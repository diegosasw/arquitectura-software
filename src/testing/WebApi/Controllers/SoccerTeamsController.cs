using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApi.Contracts;

namespace WebApi.Controllers
{
    [Route("api/[Controller]")]
    public class SoccerTeamsController 
        : ControllerBase
    {
        private readonly ITeamService _teamService;

        public SoccerTeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetTeams()
        {
            var teams = await _teamService.GetTeams();
            return Ok(teams);
        }
    }
}