using System.Collections.Generic;
using System.Threading.Tasks;
using WebApi.Contracts;
using WebApi.Models;

namespace WebApi.Services
{
    public class InMemoryTeamService
        : ITeamService
    {
        public Task<IEnumerable<Team>> GetTeams()
        {
            var teams =
                new List<Team>
                {
                    new Team("Real Madrid", 1910),
                    new Team("Athletic Bilbao", 1905),
                    new Team("Malaga CF", 1920)
                };

            return Task.FromResult((IEnumerable<Team>) teams);
        }
    }
}