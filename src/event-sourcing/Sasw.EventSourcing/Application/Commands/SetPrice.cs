using System;
using Sasw.EventSourcing.Application.Contracts;

namespace Sasw.EventSourcing.Application.Commands
{
    public class SetPrice
        : ICommand
    {
        public Guid AggregateId { get; set; }
        public decimal PriceEur { get; set; }
    }
}