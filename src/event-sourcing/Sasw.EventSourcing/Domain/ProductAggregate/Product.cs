using System;
using System.Collections.Generic;
using Sasw.EventSourcing.Domain.Contracts;
using Sasw.EventSourcing.Domain.ProductAggregate.Events;
using Sasw.EventSourcing.Domain.Service;

namespace Sasw.EventSourcing.Domain.ProductAggregate
{
    public class Product
        : AggregateRoot
    {
        private string _name;

        public Product(Guid id, string name) 
            : base(id)
        {
            var productCreated = new ProductCreated(id, name);
            ApplyDomainEvent(productCreated);
        }

        public Product(Guid id, IEnumerable<IDomainEvent> domainEvents) 
            : base(id, domainEvents)
        {
        }

        public void SetPrice(string originalCurrency, decimal originalAmount, IForexService forexService)
        {
            // business rule: only products that start with 'A' can be price set.
            // and applies current exchange rate for dollars and converts it
            var usdExchangeRate = forexService.GetCurrentUsdExchangeRate(originalCurrency);
            var amountInUsd = originalAmount * usdExchangeRate;
            if (_name.StartsWith('A'))
            {
                var priceSet = new PriceSet(Id, originalCurrency, originalAmount, amountInUsd, usdExchangeRate);
                ApplyDomainEvent(priceSet);
            }
        }
        
        protected override void RegisterDomainEventAppliers()
        {
            RegisterDomainEventApplier<ProductCreated>(Applier);
            RegisterDomainEventApplier<PriceSet>(Applier);
        }

        private void Applier(ProductCreated productCreated)
        {
            _name = productCreated.Name;
        }
        
        private void Applier(PriceSet priceSet)
        {
        }
    }
}