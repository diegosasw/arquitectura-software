﻿using System;
using Sasw.EventSourcing.Application;
using Sasw.EventSourcing.Application.CommandHandlers;
using Sasw.EventSourcing.Application.Commands;
using Sasw.EventSourcing.Application.Contracts;
using Sasw.EventSourcing.Domain;
using Sasw.EventSourcing.Domain.Contracts;
using Sasw.EventSourcing.Domain.ProductAggregate;
using Sasw.EventSourcing.Domain.Service;
using ICommand = System.Windows.Input.ICommand;

namespace Sasw.EventSourcing
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Event Sourcing sample");
            
            // Dependency injection instantiation
            IEventStore eventStore = new InMemoryEventStore();
            AggregateRootFactory<Product> productFactory = new AggregateRootFactory<Product>();
            IAggregateRepository<Product> productRepository = 
                new AggregateRepository<Product>(eventStore, productFactory);
            IDomainEventsConsumer domainEventsConsumer = new PersisterDomainEventsConsumer(eventStore);
            IForexService forexService = new DummyForexService();
            ICommandHandler<CreateProduct> createProductHandler = new CreateProductHandler(domainEventsConsumer);
            ICommandHandler<SetPrice> setPriceHandler = 
                new SetPriceHandler(productRepository, domainEventsConsumer, forexService);
            
            // Sample
            // UC - Create Product
            var createProduct =
                new CreateProduct
                {
                    Name = "Albondigas"
                };
            var aggregateId = createProductHandler.Handle(createProduct);

            var setPrice =
                new SetPrice
                {
                    AggregateId = aggregateId,
                    PriceEur = 100
                };
            setPriceHandler.Handle(setPrice);
            
            // Forex changes
            forexService.SetEurExchangeRate((decimal)2.7);

            var setNewPrice =
                new SetPrice
                {
                    AggregateId = aggregateId,
                    PriceEur = 30
                };
            setPriceHandler.Handle(setNewPrice);

            Console.WriteLine("Press ENTER to end");
            Console.ReadLine();
        }
    }
}